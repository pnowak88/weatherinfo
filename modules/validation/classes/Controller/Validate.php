<?php

    class Controller_Validate extends Controller{
        
        public function action_index(){
            if($this->request->param('external') == "external")
            {
                $this->_external();
            }
            else
            {
                $this->_std_valid();
            }
        }
        
        protected function _external(){
            try{
                $name = $this->request->param('model');                
                $strategy = $this->request->param('strategy');
                $data = $this->request->post();
                
                $words = explode("_", $name);
                foreach($words as &$w){
                    $w = ucfirst($w);
                }
                $name = implode("_",$words);

                $validator = Validator::factory($name);
                $validator->set_data($data);
                $validator->check($strategy);
                $this->response->body(json_encode(array('status'=>true)));
            }catch(Kohana_ORM_Validation_Exception $e){
                $this->response->body(json_encode(array(
                    'status' => false,
                    'errors' => $e->errors($name)
                )));
            }

        }

        protected function _std_valid(){
            try
            {
                $words = explode("_",$this->request->param('model'));
                foreach($words as &$w){
                    $w = ucfirst($w);
                }
                $model_name = implode("_",$words);
                
                $model = ORM::factory($model_name);
                
                $post = array();
                
                $model->values($this->request->post(),$model->rest_attributes());
                
                $validation = $model->rest_validation($this->request->param('strategy'));
                
                if($validation->check())
                {
                    $this->response->body(json_encode(array('status'=>true)));
                }
                else
                {
                    if(method_exists($model,'validation_messages'))
                    {
                        $model_name_lc = $model->validation_messages();
                    }
                    else
                    {
                        $words = explode("_",$this->request->param('model'));
                        foreach($words as &$w){
                            $w = lcfirst($w);
                        }
                        $model_name_lc = implode("_",$words);
                    }
                    //print_r($validation->errors('validation/'.$model_name_lc.'/_external'));exit;
                    $this->response->body(json_encode(array('status'=>false,'errors'=>$validation->errors('validation/'.$model_name_lc.'/_external'))));
                }
            }
            catch(Exception $e)
            {
                throw $e;
            }
            
        }
        
    }


?>
