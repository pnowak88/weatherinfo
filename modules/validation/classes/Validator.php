<?php

abstract class Validator{
    protected $_data = array();
    
    /**
     * 
     * @param string $name
     * @return Validator
     */
    static public function factory($name){
        $name = "Validator_".ucfirst($name);
        return new $name();
    }
    
    
    
    static public function auto_check(Request $request, $post = true){
        $name = $request->controller();
        $strategy = $request->action();
        $data = array();
        
        if($post == true)
        {
            $data = $request->post();
        }
        else{
            $data = $request->query();
        }
        
        $validator = Validator::factory($name);
        $validator->set_data($data);
        $validator->check($strategy);
    }
    
    public function rules($strategy){
        $method = 'rules_'.$strategy;
        if (method_exists($this, $method))
        {
            return $this->$method();
        }
        return array();
    }
    
    public function check($strategy){
        $v = Validation::factory($this->_data);
        $rules = $this->rules($strategy);
        
        foreach($rules as $fld => $r)
        {
            $v->rules($fld, $r);
        }
        if( ! $v->check() )
        {
            throw new ORM_Validation_Exception(get_class($this), $v);
        }
    }
    
    public function get_data()
    {
        return $this->_data;
    }

    public function set_data($data)
    {
        $this->_data = $data;
        return $this;
    }
    
    public function unique($field, $value, $model)
    {
            $model = ORM::factory($model)
                    ->where($field, '=', $value)
                    ->find();

            return ( ! $model->loaded());
    }
    
}

?>
