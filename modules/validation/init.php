<?php defined('SYSPATH') or die('No direct script access.');

Route::set('validation', 'validate(/<model>(/<strategy>(/<external>)))')
	->defaults(array(
		'controller' => 'validate',
		'action'     => 'index',
	));

