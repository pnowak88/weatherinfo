<div data-type="weatherinfo_widget" data-city="<?php echo $city;?>">
    <div>
        <h4>WeatherInfo</h4>
        <?php echo Form::select('city', $cities, $city);?>
    </div>
    <div class="weatherinfo_loader">
        <strong>Loading WeatherInfo Widget, please wait a moment</strong>
        <?php echo HTML::image('assets/frontend/img/ajax-loader-1.gif');?>
    </div>
    <div class="weatherinfo_content">
        <h4></h4>
    </div>
</div>
