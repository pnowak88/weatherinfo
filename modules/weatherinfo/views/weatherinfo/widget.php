<?php foreach ($conditions as $key=>$c):?>
    <?php if ($c):?>
    <div>
        <?php echo HTML::image('assets/weatherinfo/img/'.$key.'.png');?>
        <strong><?php echo str_replace('_', ' ', $key);?>:</strong>
        <?php echo $c;?>
    </div>
    <?php endif;?>
<?php endforeach;?>