<?php

/**
 * Controller responsible for WeatherInfo actions
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Controller_Weather extends Controller{
    
   /**
    * Searchs cities by country name and returns results as json encoded array.
    * For example to provide data for autocomplete
    * 
    * @since 1.0.0
    */
    public function action_search_city_json()
    {
        $term = $this->request->query('term');
        
        try
        {
            // Produce actually configured Weather Service
            $weather_service = Weather_Service::factory();
            // Search cities by country name
            $json = $weather_service->search_city_json($term);
        }
        catch(SoapFault $e)
        {
            Kohana::$log->add(Log::NOTICE, 'SOAP fault in searching cities: '.$e->getMessage());
            $json = json_encode(array());
        }
        
        // Send response for autocomplete
        $this->response->headers('Content-Type','application/json');
        $this->response->body($json);
    }
    
   /**
    * Render weather widget
    * 
    * @since 1.0.0
    */
    public function action_widget()
    {
        $city_id = $this->request->param('id');
        $city_obj = NULL;
        
        if ( !$city_id)
        {
            $city_obj = ORM::factory('City')->find();
            
            if ($city_obj->loaded())
            {
                $city_id = $city_obj->get_id();
            }
            else
            {
                $city_id = ORM::factory('City')->find();
                $city_id = $city->get_id();
            }
        }
        else
        {
            $city_obj = ORM::factory('City', $city_id);
        }
        
        // Return main content of widget
        if ($this->request->is_ajax())
        {              
            // Produce actually configured Weather Service
            $weather_service = Weather_Service::factory();
            
            // Get city weather
            try
            {
                $json = $weather_service->get_city_weather_json($city_obj->get_name());
                
                // Cache received data
                $city_weather_data = new Model_City_Weather_Data;
                $city_weather_data->values((array)json_decode($json));
                $city_weather_data->set_city_id($city_obj->get_id());
                $city_weather_data->save();
                
                $conditions = json_decode($json);
                Kohana::$log->add(Log::NOTICE, 'data from WebService');
            }
            catch(SoapFault $e)
            {
                Kohana::$log->add(Log::NOTICE, 'data from DB after SOAP timeout: '.$e->getMessage());
                // If timeout read from database
                $last_weather_conditions = $city_obj->city_weather_datas->order_by('id', 'DESC')->find()->as_array();
                unset($last_weather_conditions['id']);
                unset($last_weather_conditions['city_id']);
                $conditions = $last_weather_conditions;
            }
            
           
            
            // Load view
            $view = View::factory('weatherinfo/widget')
                        ->bind('conditions', $conditions);
        }
        else // Return preloader
        {
            // Get available cities
            $cities = ORM::factory('City')
                        ->where('active', '=', true)
                        ->order_by('name', 'ASC')
                        ->find_all()
                        ->as_array('id', 'name');
            
            $view = View::factory('weatherinfo/widget_loader')
                        ->bind('city', $city_id)
                        ->bind('cities', $cities);
        }
        
        $this->response->body($view);
    }
    
} // End Controller_Weather
?>
