<?php

/**
 * Abstraction level of weather services
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
abstract class Weather{
    
    protected static $_config;
    protected $_api;

    protected $_weather_conditions = array(
                'wind' => NULL,
                'visibility' => NULL,
                'temperature' => NULL,
                'dew_point' => NULL,
                'relative_humidity' => NULL,
                'pressure' => NULL,
                'sky_conditions' => NULL,
              );
    
    protected abstract function get_api();
}

?>
