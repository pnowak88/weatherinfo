<?php

/**
 * This interface must be implemented by all Weather Services,
 * and includes necessary to weather widget methods.
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
interface Weather_Interface{
    
    /**
     * Implements cities names feed for admin
     * 
     * @param string $city_query City search phrase
     * @return json encoded array of city names
     */
    public function search_city_json($city_query);
    
    /**
     * Implements city weather feed for widget
     * 
     * @param string $city_query City search phrase
     * @return json encoded array of weather conditions
     */
    public function get_city_weather_json($city_name);
}
?>
