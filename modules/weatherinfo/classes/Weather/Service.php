<?php

/**
 * Base class of each Weather Services Drivers
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Weather_Service extends Weather{
    
    public function __construct()
    {
        self::$_config = Kohana::$config->load("weatherinfo");
        
        // SOAP timeout configuration
        ini_set('default_socket_timeout', self::$_config['soap_timeout']);
        $params = array('connection_timeout'=>self::$_config['soap_timeout']);
        $this->_api = new SoapClient(self::$_config['url'], $params);
    }
    
    /**
    * Produces selected Weather Service object
    * 
    * @version 1.0.0
    * @return mixed (actieve service object)
    */
    public static function factory()
    {
        self::$_config = Kohana::$config->load("weatherinfo");
        $class = 'Service_'.self::$_config['service'];
        $service = new $class;
        
        return $service;
    }
    
    /**
     * Returns SoapClient
     * 
     * @return SoapClient
     */
    public function get_api() {
        return $this->_api;
    }
}
?>
