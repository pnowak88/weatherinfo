<?php

/**
 * Global Weather Web Service driver for WeatherInfo widget
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Service_Global_Weather extends Weather_Service implements Weather_Interface{
    
    /**
     * Implements cities names feed for admin
     * 
     * @param string $city_query City search phrase
     * @return json encoded array of city names
     */
    public function search_city_json($country_query) {
        $result = $this->_get_cities_by_country($country_query);
        $cities_json = $this->_prepare_cities_json($result);
                
        return $cities_json;
    }
    
    /**
     * Implements city weather feed for widget
     * 
     * @param string $city_query City search phrase
     * @return json encoded array of weather conditions
     */
    public function get_city_weather_json($city_name)
    {
        $result = $this->_get_weather($city_name);
        $weather_conditions_json = $this->_prepare_weather_conditions_json($result);
                
        return $weather_conditions_json;
    }
    
    /**
     * Communication with WS, provide cities names
     * 
     * @param string $country_query
     * @return string
     */
    protected function _get_cities_by_country($country_query)
    {
        $result = $this->get_api()->getCitiesByCountry(array('CountryName' => $country_query));
        return $result;
    }
    
    /**
     * Communication with WS, provide weather data
     * 
     * @param string $city_name
     * @return string
     */
    protected function _get_weather($city_name)
    {
        $result = $this->get_api()->getWeather(array('CityName' => $city_name, 'CountryName' => ''));
        return $result;
    }
    
    /**
     * Preparation of cities data
     * 
     * @param string $result
     * @return json encoded array
     */
    protected function _prepare_cities_json($result)
    {
        $cities = array();
        $xml = new SimpleXMLElement($result->GetCitiesByCountryResult);

        foreach ($xml->Table as $c)
        {
            $cities[] = (String)$c->City;
        }
        
        sort($cities);
        $cities_json = json_encode($cities);
        
        return $cities_json;
    }
    
    /**
     * Preparation of weather data
     * 
     * @param string $result
     * @return json encoded array
     */
    protected function _prepare_weather_conditions_json($result)
    {
        $weather_result = trim(preg_replace('/<\?xml .*\?>/', '', $result->GetWeatherResult));
        $current_weather = simplexml_load_string($weather_result);

        if (isset($current_weather->Wind))
            $this->_weather_conditions['wind'] = $this->_parse_wind((string)$current_weather->Wind);
        if (isset($current_weather->Visibility))
            $this->_weather_conditions['visibility'] = $this->_parse_visibility((string)$current_weather->Visibility);
        if (isset($current_weather->Temperature))
            $this->_weather_conditions['temperature'] = $this->_parse_temperature((string)$current_weather->Temperature);
//        if (isset($current_weather->DewPoint))
//            $this->_weather_conditions['dew_point'] = $this->_parse_dew_point((string)$current_weather->DewPoint);
        if (isset($current_weather->RelativeHumidity))
            $this->_weather_conditions['relative_humidity'] = $this->_parse_relative_humidity((string)$current_weather->RelativeHumidity);
        if (isset($current_weather->Pressure))
            $this->_weather_conditions['pressure'] = $this->_parse_pressure((string)$current_weather->Pressure);
        if (isset($current_weather->SkyConditions))
            $this->_weather_conditions['sky_conditions'] = $this->_parse_sky_conditions((string)$current_weather->SkyConditions);

        $weather_conditions_json = json_encode($this->_weather_conditions);
        
        return $weather_conditions_json;
    }
    
    // Group of function parsing weather conditions
    
    protected function _parse_wind($wind)
    {
        $wind = str_replace(':0', '', $wind);
        $wind = trim(preg_replace('/\(.*\)/U', '', $wind));

        return $wind;
    }
    
    protected function _parse_visibility($visibility)
    {
        $visibility = str_replace(':0', '', $visibility);

        return $visibility;
    }
    
    protected function _parse_temperature($temperature)
    {
        preg_match('/\(.*\)/', $temperature, $matches);
        $temperature = substr($matches[0], 1, -1);
        return $temperature;
    }
    
    protected function _parse_dew_point($dew_point)
    {
        preg_match('/\(.*\)/', $dew_point, $matches);
        $dew_point = substr($matches[0], 1, -1);
        return $dew_point;
    }
    
    protected function _parse_relative_humidity($relative_humidity)
    {
        return $relative_humidity;
    }
    
    protected function _parse_pressure($pressure)
    {
        preg_match('/\(.*\)/', $pressure, $matches);
        $pressure = substr($matches[0], 1, -1);
        return $pressure;
    }
    
    protected function _parse_sky_conditions($sky_conditions)
    {
        return $sky_conditions;
    }
    
}
?>
