Loader = {
    
    popup_cache:      {},
    template_cache:   {},
    
    popup: function(path, cssClass, callback){
        var popup = this.popup_cache[path];
        var that = this;
        
        if (typeof cssClass == 'undefined') cssClass = 'small';
        
        path = path.replace(/\//g,'-');
        if (!popup) {
            $.get(Simplemo.request.base+"partial/popup/"+path+'/'+cssClass, function(html) {
                that.popup_cache[path] = html;
                callback(that.popup_cache[path]);
            });
        }
        else{
            callback(popup);
        }
    },
    
    view: function(path,callback){
        path = path.replace(/\//g,'-');
        $.get(Simplemo.request.base+"partial/view/"+path, function(html) {
            callback(html);
        });
    },
    
    template: function(path,callback){
        var template = this.template_cache[path];
        var that = this;
        path = path.replace(/\//g,'-');
        if (!template) {
            $.get(Simplemo.request.base+"partial/template/"+path, function(html) {
                that.template_cache[path] = html;
                callback(that.template_cache[path]);
            });
        }
        else{
            return callback(template);
        }
    }
   
};