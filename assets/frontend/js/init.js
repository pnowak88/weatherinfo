$(function(){
    
    require([ 
        'weatherinfo/js/widget/body'
    ], function(View_Widget){

        var Init = Backbone.View.extend({

            initialize: function(){
                
                // Array of weather widgets
                WeatherInfo.views.widgets = [];
                
                // Load Backbone View for WeatherInfo Widgets
                $('[data-type="weatherinfo_widget"]').each(function(){
                    WeatherInfo.views.widgets.push(new View_Widget({el: $(this)}));
                });

            }

        });

        WeatherInfo.views = {};
        WeatherInfo.views.init = new Init();

    });
});
