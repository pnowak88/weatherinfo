$(function(){
    
    require([ 
        'backend/js/validator'
    ], function(View_Validator){

        var Init = Backbone.View.extend({

            initialize: function(){

                // Add JS Validator to forms
                if ($('[data-valid="true"]').length){
                    $('[data-valid="true"]').each(function(){
                        new View_Validator({el: $(this).parent()});
                    });
                }
                
                // Load Backbone View for current page
                require(['backend/js/views/'+WeatherInfo.request.uri+'/body'], function(View){ 

                    if(typeof View != "undefined") {
                        WeatherInfo.views.body = new View({ el: $('body') });
                    }

                });
                

            }

        });

        WeatherInfo.views = {};
        WeatherInfo.views.init = new Init();

    });
});
