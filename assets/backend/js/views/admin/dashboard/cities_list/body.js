define([], function(){
    
        Cities_List_View = Backbone.View.extend({

            initialize: function(){
                
            },
            
            events: function(){
                return {
                    'click [data-event="delete_city"]': 'event_delete_city'
                };
            },
                    
            event_delete_city: function(e){
                var $el = $(e.currentTarget);
                var $link = $('#myModal').find('a.btn-danger');
                $link.attr('href', $el.attr('href'));
                $('#myModal').modal();
                return false;
            }
                    
        });
    
        return Cities_List_View;
    
    });

