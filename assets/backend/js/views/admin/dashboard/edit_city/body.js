define([], function(){
    
        Add_City_View = Backbone.View.extend({

            initialize: function(){
                var that = this;
                this.cache = {};

                this.$el.find( '[name="name"]' ).autocomplete({
                    minLength: 0,
                    source: function(request, response){
                        return that.search_city_source(request, response)
                    },
                    focus: function(event, ui){
                        return that.search_city_focus(event, ui);
                    },
                    select: function(event, ui){
                        return that.search_city_select(event, ui);
                    },
                    response: function(event, ui){
                        return that.search_city_response(event, ui)
                    }
                    
                })
               
            },
            
            events: function(){
                return {};
            },
            
            search_city_source: function( request, response ) {
                var self = this;
                var term = request.term;
                
                this.$el.find('.control-group img').show();
                
                if ( term in this.cache ) {
                    response( this.cache[ term ] );
                    return;
                }

                $.getJSON( WeatherInfo.request.base + 'weather/search_city_json', request, function( data, status, xhr ) {
                    self.cache[ term ] = data;
                    if( !data.length ){
                        data = [{
                            empty: true
                        }];
                    }
                    response( data );
                });
            },
                    
            search_city_focus: function( event, ui ) {
                $( '[name="name"]' ).val( ui.item.label );
                return false;
            },
                    
            search_city_select: function( event, ui ) {
                this.$el.find('.control-group img').hide();
                return false;
            },
                    
            search_city_response: function( event, ui ) {
                
                var self = this;
                
                if(typeof ui.content[0].empty === 'undefined'){
                    this.$el.find( '[name="name"]' ).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                        self.client_id = item.id;
                        return $( "<li>" )
                        .append( "<a>" + item.name + " " + item.surname + "<br/>" + item.telephone + " " + item.email + "</a>" )
                        .appendTo( ul );
                    };
                }
            }
                    
        });
    
        return Add_City_View;
    
    });

