define([], function(){

    var View_Validator = Backbone.View.extend({
        
        events: function(){
            return {
                "submit [data-valid='true']"    : "_valid"
            };
        },
        
        _valid: function(e){
            var $target = $(e.currentTarget);

            if($target.hasClass("enable"))
                return true; 

            type = $target.attr("data-valid-type");
            strategy = $target.attr("data-valid-strategy");
            
            err = $target.find('.error');
            err.find('.error_msg').remove();
            err.removeClass('error');
            
            $.ajax( WeatherInfo.request.base + "validate/" + type + '/' + strategy + '/external', {
                data: $target.serialize(),
                type: 'POST'
            }).done(function(response){
                var data = jQuery.parseJSON(response);
                for(key in data.errors){
                    
                    field = $target.find('[name="'+key+'"]');

                    field.closest('div').addClass('error');
                    field.closest('.control-group').addClass('error');
                    $(field).closest('div').append('<span class="error_msg">'+data.errors[key]+'</span>')
                }

                if(data.status == true){
                    $target.addClass("enable");
                    $target.submit();
                }
                
            });
            
            return false;
        },
        
        
    });

    return View_Validator;
});
    