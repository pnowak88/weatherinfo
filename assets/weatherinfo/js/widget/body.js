define([], function(){
    
        View_Widget = Backbone.View.extend({

            initialize: function(){
                var that = this;
                var city = this.$el.attr('data-city');
                
                $.get( WeatherInfo.request.base + 'weather/widget/'+city, function( data ) {
                    that._update_widget(data);
                });
               
                setInterval(this.event_update_widget.bind(this), WeatherInfo.configs.interval);
            },
            
            events: function(){
                return {
                    "change [name='city']": "event_change_city"
                };
            },
                    
            _interval_refresh: function(){
        
            },
                    
            _update_widget: function(data){
                this.$el.find('.weatherinfo_loader').hide();
                this.$el.find('.weatherinfo_content').html(data);
                this.$el.find('.weatherinfo_content').animate({opacity: 1.0});
            },
                    
            event_change_city: function(e){
                var $el = $(e.currentTarget);
                var that = this;
                var city = $el.val();
                
                this.$el.find('.weatherinfo_loader').show();
                this.$el.find('.weatherinfo_content').animate({opacity: 0.5});
                
                $.get( WeatherInfo.request.base + 'weather/widget/'+city, function( data ) {
                    that._update_widget(data);
                });
            },
                    
            event_update_widget: function(e){
                var $el = this.$el.find('select');
                var that = this;
                var city = $el.val();
                
                this.$el.find('.weatherinfo_content').animate({opacity: 0.5});
                
                $.get( WeatherInfo.request.base + 'weather/widget/'+city, function( data ) {
                    that._update_widget(data);
                });
            }
            
        });
    
        return View_Widget;
    
    });

