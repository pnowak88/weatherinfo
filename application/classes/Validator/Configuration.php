<?php
/**
 * Configuration form validator
 * 
 * @since 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Validator_Configuration extends Validator{

    public function rules_default(){
        return array(
            'service_url' => array(
                array('not_empty'),
                array('url')
            ),
            'soap_timeout' => array(
                array('not_empty'),
                array('numeric'),
            )
        );
    }

}

?>
