<?php
/**
 * City form validator
 * 
 * @since 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Validator_City extends Validator{

    public function rules_default(){
        return array(
            'name' => array(
                array('not_empty'),
            )
        );
    }

}

?>
