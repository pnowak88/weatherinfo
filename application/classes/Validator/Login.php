<?php
/**
 * Login form validator
 * 
 * @since 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Validator_Login extends Validator{

    public function rules_default(){
        return array(
            'username' => array(
                array('not_empty')
            ),
            'password' => array(
                array('not_empty'),
                array(array($this, 'check_login_data'),array(':validation', ':value'))
            )
        );
    }

    /**
     * Checks login  data
     **/
    public function check_login_data(Validation $valid, $value){

       $user = ORM::factory('User')
                ->where('username', '=', $this->_data['username'])
                ->find();

       if( !$user->loaded() || $user->password !=  Auth::instance()->hash($value))
       {
           $valid->error('password', 'incorrect_login_data');
       }
    }
}

?>
