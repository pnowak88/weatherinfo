<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Controller responsible for Auth actions
 * login/logout
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Controller_Admin_Dashboard extends Controller_Admin_Main {
        
        /**
         * Runs before each action
         * 
         * @since 1.0.0
         */
        public function before()
        {
            parent::before();
            $this->user = NULL;
                    
            // If logged user hasn't sysadmin role -> logout
            if ( !Auth::instance()->logged_in('admin'))
            {
                $this->redirect('admin/auth/logout');
            }
            else
            {            
                $this->user = Auth::instance()->get_user();
            }
            
            // Load Admins decorator view
            $this->template->content = View::factory('admin/dashboard/decorator')
                                            ->bind('user', $this->user);
        }
        
        /**
         * Main administrative page
         * 
         * @since 1.0.0
         */
	public function action_index()
	{
            $this->redirect('admin/dashboard/cities_list');
	}
        
        /**
         * Cities management page (CRUD operations)
         * 
         * @since 1.0.0
         */
	public function action_cities_list()
	{
            $cities = ORM::factory('City')->order_by('name', 'ASC')->find_all();
            
            $view = View::factory('admin/dashboard/city/list')
                        ->bind('cities', $cities);
            $this->template->content->view = $view;
	}
        
        /**
         * Adding city page
         * 
         * @since 1.0.0
         */
	public function action_add_city()
	{
            // Load form data from session
            $form_data = Session::instance()->get_once('form_data', array(array(), array()));
            
            $view = View::factory('admin/dashboard/city/form')
                        ->bind('errors', $form_data[1])
                        ->bind('values', $form_data[0]);
            $this->template->content->view = $view;
	}
        
        /**
         * Edit city page
         * 
         * @since 1.0.0
         */
	public function action_edit_city()
	{
            $city_id = $this->request->param('arg1');
            $city = new Model_City($city_id);
            
            // Load form data from session
            $form_data = Session::instance()->get_once('form_data', array($city->as_array(), array()));
            
            $view = View::factory('admin/dashboard/city/form')
                        ->bind('city', $city)
                        ->bind('errors', $form_data[1])
                        ->bind('values', $form_data[0]);
            $this->template->content->view = $view;
	}
        
        /**
         * System configuration page
         * 
         * @since 1.0.0
         */
	public function action_configuration()
	{
            // Get all settings
            $config = Model_Config::get_val();
            // Load form data from session
            $form_data = Session::instance()->get('form_data', array(array(), array()));
            
            $view = View::factory('admin/dashboard/configuration')
                        ->bind('config', $config)
                        ->bind('errors', $form_data[1])
                        ->bind('values', $form_data[0]);
            $this->template->content->view = $view;
	}
        
        /**
         * Save configuration
         * 
         * @since 1.0.0
         */
	public function action_save_configuration()
	{
            try
            {
                // Get POST values
                $post_data = $this->request->post();
                $validator = Validator::factory('configuration');
                $validator->set_data($post_data);
                $validator->check('default');

                // Update settings
                Model_Config::set_val('service_url', $post_data['service_url']);
                Model_Config::set_val('soap_timeout', $post_data['soap_timeout']);

                Flash::add('Data saved.', 'success');
                $this->redirect('admin/dashboard/configuration');
            }
            catch(ORM_Validation_Exception $e)
            {
                // Handle errors
                $errors = $e->errors('configuration');
                Flash::add('Form contains errors.');
                Session::instance()->set('form_data', array($post_data, $errors));
                $this->redirect($this->request->referrer());
            }
	}
        
        /**
         * Save city
         * 
         * @since 1.0.0
         */
	public function action_save_city()
	{
            try
            {
                // Get POST values
                $post_data = $this->request->post();
                $validator = Validator::factory('city');
                $validator->set_data($post_data);
                $validator->check('default');

                $city_id = $this->request->param('arg1');
                $city = new Model_City($city_id);
                
                if ( !$city->loaded())
                {
                    // Create new City object
                    $city = new Model_City;
                }
                
                $city->values($post_data);
                $city->save();

                Flash::add('Data saved.', 'success');
                $this->redirect('admin/dashboard/cities_list');
            }
            catch(ORM_Validation_Exception $e)
            {
                // Handle errors
                $errors = $e->errors('city');
                Flash::add('Form contains errors.');
                Session::instance()->set('form_data', array($post_data, $errors));
                $this->redirect($this->request->referrer());
            }
	}

} // End Controller_Admin_Dashboard
