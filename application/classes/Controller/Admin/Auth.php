<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Controller responsible for Auth actions
 * login/logout
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Controller_Admin_Auth extends Controller_Admin_Main {
        
        /**
         * Runs before each action
         * 
         * @since 1.0.0
         */
        public function before()
        {
            parent::before();
            $this->template->assets = Assets::factory()->group('login'); 
        }
        
        /**
         * Actually unused...
         * 
         * @since 1.0.0
         */
	public function action_index()
	{
            $this->redirect('admin/auth/login');
        }
        
        /**
         * Login page
         * 
         * @since 1.0.0
         */
	public function action_login()
	{
            if (Auth::instance()->logged_in())
            {
                $this->redirect('admin/dashboard');
            }
            
            $form_data = Session::instance()->get('form_data', array(array(), array()));
            $view = View::factory('admin/auth/login')
                        ->bind('errors', $form_data[1])
                        ->bind('values', $form_data[0]);
            $this->template->content = $view;
	}
        
        /**
         * Logins to the Admin Panel
         * 
         * @since 1.0.0
         */
        public function action_do_login()
	{
            try
            {
                // Validate POST data
                $post_data = $this->request->post();
                $validator = Validator::factory('login');
                $validator->set_data($post_data);
                $validator->check('default');
                
                // Try to log in
                if(Auth::instance()->login($post_data['username'], $post_data['password']))
                {
                    $this->redirect('admin/dashboard');
                }
                else
                {
                    // On failure redirect to login page
                    $this->redirect('admin/auth/login');
                }
            }
            catch(ORM_Validation_Exception $e)
            {
                // Handle login errors
                $errors = $e->errors('login');
                Session::instance()->set('form_data', array($post_data, $errors));
                $this->redirect('admin/auth/login');
            }
	}
        
        /**
         * Logouts from system
         * 
         * @since 1.0.0
         */
        public function action_logout()
	{
            // Logout and redirect to specified url
            Auth::instance()->logout();
            $this->redirect('admin/auth/login');
	}

} // End Controller_Admin_Auth
