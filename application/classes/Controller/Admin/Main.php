<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Base Admin Controller
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Controller_Admin_Main extends Controller_Template {
        
        /**
         * Runs before each action
         * 
         * @since 1.0.0
         */
        public function before()
        {
            parent::before();
            $this->user = NULL;
                    
            // If logged user hasn't sysadmin role -> logout
            if ( !Auth::instance()->logged_in('admin') && $this->request->controller() !== 'Auth')
            {
                $this->redirect('admin/auth/logout');
            }
            else
            {            
                $this->user = Auth::instance()->get_user();
            }
            $this->template = View::factory('admin/template');
            $this->template->assets = Assets::factory()->group('backend');

        }

} // End Controller_Admin_Main
