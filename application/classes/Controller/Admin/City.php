<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Controller responsible for Administrative City actions
 * login/logout
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Controller_Admin_City extends Controller_Admin_Main {
        
        /**
         * Runs before each action
         * 
         * @since 1.0.0
         */
        public function before()
        {
            parent::before();
            // Load Admins decorator view
            $this->template->content = View::factory('admin/dashboard/decorator')
                                            ->bind('user', $this->user);
        }
        
        /**
         * Action responsible for activating/deactivating cities
         * 
         * @since 1.0.0
         */
	public function action_toggle_active()
	{
            $id = $this->request->param('arg1');
            if ($id)
            {
                $city = new Model_City($id);
                
                if ($city->loaded())
                {
                    $city->toggle_active();
                    Flash::add('Toggle active completed.', 'success');
                    $this->redirect($this->request->referrer());
                }
            }
            
            Flash::add('Toggle active error.');
            $this->redirect($this->request->referrer());
	}
        
        /**
         * Action responsible for deleting cities
         * 
         * @since 1.0.0
         */
	public function action_delete()
	{
            $id = $this->request->param('arg1');
            if ($id)
            {
                $city = new Model_City($id);
                
                if ($city->loaded())
                {
                    $city->delete();
                    Flash::add('Delete completed.', 'success');
                    $this->redirect($this->request->referrer());
                }
            }
            
            Flash::add('Delete error.');
            $this->redirect($this->request->referrer());
	}

} // End Controller_Admin_City
