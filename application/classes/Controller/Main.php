<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Base Frontend Controller
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Controller_Main extends Controller_Template {
        
        /**
         * Runs before each action
         * 
         * @since 1.0.0
         */
        public function before() {
            parent::before();
            $this->template->assets = Assets::factory()->group('frontend');
        } 

        /**
         * Frontend Main Page
         * 
         * @since 1.0.0
         */
	public function action_index()
	{
            $weather_widget1 = Request::factory('weather/widget/')->execute();
            $weather_widget2 = Request::factory('weather/widget/12')->execute();
            
            // Set interval for refreshing wisgets
            Javascript::instance()->configs->interval = Kohana::$config->load('weatherinfo.refresh');
            
            $view  = View::factory('frontend/index')
                        ->bind('weather_widget1', $weather_widget1)
                        ->bind('weather_widget2', $weather_widget2);
            $this->template->content = $view;
	}

} // End Controller_Main
