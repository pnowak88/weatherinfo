<?php

   /**
    * City model
    * 
    * @version 1.0.0
    * @author Paweł Nowak <pawel88nowak@gmail.com>
    */
    class Model_City extends ORM
    {
        // Associations
        protected $_has_many = array(
                        'city_weather_datas' => array(),
                  );
        
        public function get_name()
        {
            return $this->name;
        }
        
        public function get_id()
        {
            return $this->id;
        }
        
        public function is_active()
        {
            return $this->active;
        }
        
        public function toggle_active()
        {
            $this->active = !$this->active;
            $this->save();
        }
    }
?>