<?php

/**
 * Model representing User in DB
 * 
 * @version 1.0.0
 * @author Paweł Nowak <pawel88nowak@gmail.com>
 */
class Model_User extends Model_Auth_User
{
    // Associations
    protected $_has_many = array(
        'user_tokens' => array('model' => 'User_Token'),
        'roles'       => array('model' => 'Role', 'through' => 'roles_users'),
    );
    
    
    /**
     * USername getter
     * 
     * @return string username
     */
    public function get_username()
    {
        return $this->username;
    }
}// End Model_User
