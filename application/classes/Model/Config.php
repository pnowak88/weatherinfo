<?php

   /**
    * Settings model
    * gets and sets key-value pairs responsible for system settings
    * 
    * @version 1.0.0
    * @author Paweł Nowak <pawel88nowak@gmail.com>
    */
    class Model_Config extends ORM
    {
        
        /**
         * Geter of settings
         * @param int $key - setting key
         * @return mixed Database_MySQL_Result or Model_Config
         */
         public static function get_val($key = NULL)
         {
            $settings = ORM::factory('Config');
            
            if(!empty($key))
            {
                $settings = $settings->where("key", "=", $key);
                return $settings->find();
            }
            else
            {
                return $settings->find_all()->as_array('key', 'value');
            }
         }

        /**
         * Updates settings in DB
         * @param int $key - setting key
         * @param int $value - setting value
         */
         public static function set_val($key, $value)
         {
            $setting = ORM::factory('Config')->where("key", "=", $key);
            $setting->find();

            // If setting exists  - set value
            if($setting->loaded())
            {
                $setting->value = $value; 
            }
            else // Create setting
            {
                $setting->key = $key;
                $setting->value = $value;
            }
            $setting->save();
         }
    }
?>


