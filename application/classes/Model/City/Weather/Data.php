<?php

   /**
    * City_Weather_Data model
    * 
    * @version 1.0.0
    * @author Paweł Nowak <pawel88nowak@gmail.com>
    */
    class Model_City_Weather_Data extends ORM
    {
        
        // Setter of city_id
        public function set_city_id($city_id)
        {
            $this->city_id = $city_id;
        }

    }
?>