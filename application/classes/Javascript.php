<?php

/**
 * Responsible for managing JS variables, templates
 */
class Javascript
{
    protected static $_instance = null;
    protected $_data;

    /**
     * 
     * @return Javascript
     */
    public static function instance()
    {
        if(self::$_instance==null)
        {
            self::$_instance = new Javascript();
        }
        return self::$_instance;
    }

    public function __construct($add_request = true) 
    {
        $this->_data                = new stdClass();
        $this->_data->collections   = new stdClass();
        $this->_data->models        = new stdClass();
        $this->_data->views         = new stdClass();
        $this->_data->templates     = new stdClass();
        $this->_data->configs       = new stdClass();
        if($add_request)
        {
            $this->add_request();
        }
    }

    public function __set($name, $value) 
    {
        $this->_data->$name = $value;
    }

    public function __get($name)
    {
        if(!isset($this->_data->$name))
        {
            $this->_data->$name = new stdClass();
        }
        return $this->_data->$name;
    }

    public function add_request()
    {
        $this->request->base        = URL::base(Kohana::$config->load('protocol.protocol'));
        $this->request->uri         = str_replace('//', '/', strtolower((Request::initial()->directory()?Request::initial()->directory().'/':'').Request::initial()->controller().'/'.Request::initial()->action()));
        $this->request->full_uri    = Request::initial()->uri();
        $this->request->directory   = Request::initial()->directory();
        $this->request->controller  = Request::initial()->controller();
        $this->request->action      = Request::initial()->action();
    }

    public function get_data()
    {
        return $this->_data;
    }

    public function get_serialized_data()
    {
        return json_encode($this->get_data());
    }

    public function add_template($path,$name = null)
    {
        $name = ($name!=null) ? $name : path;
        $this->_data->templates->$name = View::factory($path)->render();
    }

}