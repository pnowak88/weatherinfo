<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * This is where assets and asset dependencies are defined.
 *
 * @package    YurikoCMS
 * @author     Lorenzo Pisani - Zeelot
 * @copyright  (c) 2008-2010 Lorenzo Pisani
 * @license    http://yurikocms.com/license
 */

$i = 0;

return array
(
    'login' => array(
        
        // jQuery
        array('script', 'assets/jquery/jquery-1.7.2.min.js',                                 'body', $i++),
        
        // Backbone
        array('script', 'assets/backbone/json2.js',                                          'body', $i++),
        array('script', 'assets/backbone/underscore.js',                                     'body', $i++),
        array('script', 'assets/backbone/backbone.js',                                       'body', $i++),
        
        // Require
        array('script', 'assets/require/require.js',                                         'body', $i++),
        array('script', 'assets/require/configuration.js',                                   'body', $i++),
        
        // backend
        array('style', 'assets/backend/css/style.css',                                       'head', $i++),
        array('script', 'assets/backend/js/loader.js',                                          'body', $i++),
        array('script', 'assets/backend/js/init.js',                                            'body', $i++),
        
        // bootstrap
        array('style',  'assets/bootstrap/css/bootstrap-spacelab.css',                       'head', $i++),
        array('style',  'assets/bootstrap/css/charisma-app.css',                             'head', $i++),
        array('style',  'assets/bootstrap/css/bootstrap-responsive.css',                     'head', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-tooltip.js',                         'body', $i++),
    ),  
    
    'backend' => array(
        
        // jQuery
        array('script', 'assets/jquery/jquery-1.7.2.min.js',                                     'body', $i++),
        
        // Backbone
        array('script', 'assets/backbone/json2.js',                                          'body', $i++),
        array('script', 'assets/backbone/underscore.js',                                     'body', $i++),
        array('script', 'assets/backbone/backbone.js',                                       'body', $i++),
        
        // Require
        array('script', 'assets/require/require.js',                                         'body', $i++),
        array('script', 'assets/require/configuration.js',                                   'body', $i++),
        
        // backend
        array('style', 'assets/backend/css/style.css',                                       'head', $i++),
        array('script', 'assets/backend/js/loader.js',                                          'body', $i++),
        array('script', 'assets/backend/js/init.js',                                            'body', $i++),
        
        // bootstrap
        array('style',  'assets/bootstrap/css/bootstrap-spacelab.css',                       'head', $i++),
        array('style',  'assets/bootstrap/css/charisma-app.css',                             'head', $i++),
        array('style',  'assets/bootstrap/css/bootstrap-responsive.css',                     'head', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-tooltip.js',                         'body', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-dropdown.js',                        'body', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-alert.js',                           'body', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-modal.js',                           'body', $i++),
        
        // jQuery UI
        array('style',  'assets/jqueryui/css/jquery-ui-1.8.21.custom.css',                   'head', $i++),
        array('script',  'assets/jqueryui/js/jquery-ui-1.8.21.custom.min.js',                'body', $i++),
    ),
    
    'frontend' => array(
        
        // jQuery
        array('script', 'assets/jquery/jquery-1.7.2.min.js',                                 'body', $i++),
        
        // Backbone
        array('script', 'assets/backbone/json2.js',                                          'body', $i++),
        array('script', 'assets/backbone/underscore.js',                                     'body', $i++),
        array('script', 'assets/backbone/backbone.js',                                       'body', $i++),
        
        // Require
        array('script', 'assets/require/require.js',                                         'body', $i++),
        array('script', 'assets/require/configuration.js',                                   'body', $i++),
        
        // Frontend
        array('script', 'assets/frontend/js/loader.js',                                      'body', $i++),
        array('script', 'assets/frontend/js/init.js',                                        'body', $i++),
        
        // template
        array('style',  'assets/frontend/css/style.css',                                     'head', $i++),
        
        // bootstrap
        array('style',  'assets/bootstrap/css/bootstrap-responsive.css',                     'head', $i++),
        array('style',  'assets/bootstrap/css/bootstrap.css',                                'head', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-tooltip.js',                         'body', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-dropdown.js',                        'body', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-alert.js',                           'body', $i++),
        array('script',  'assets/bootstrap/js/bootstrap-modal.js',                           'body', $i++),
        
    ),
    
);
