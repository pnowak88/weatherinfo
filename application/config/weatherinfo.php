<?php

// Configuration of Weather Services
return array(
    'url' => Model_Config::get_val('service_url') ? Model_Config::get_val('service_url')->value : 'http://www.webservicex.com/globalweather.asmx?wsdl',
    'soap_timeout' => Model_Config::get_val('soap_timeout') ? Model_Config::get_val('soap_timeout')->value :  5,
    'service' => 'Global_Weather',
    'refresh' => 10000
);
?>
