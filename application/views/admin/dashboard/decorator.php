<!-- topbar starts -->
<div class="navbar">
        <div class="navbar-inner">
                <div class="container-fluid">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="<?php echo URL::site('admin/dashboard');?>"> 
                            <img alt="Charisma Logo" src="<?php echo URL::base();?>assets/backend/img/wi-logo.png" /> 
                            <span>WeatherInfo</span>
                        </a>

                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right" >
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="icon-user"></i><span class="hidden-phone"> <?php echo $user->get_username();?></span>
                                        <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                        <li><a href="<?php echo URL::site('/');?>">Public page</a></li>
                                        <li class="divider"></li>
                                        <li><a href="<?php echo URL::site('admin/auth/logout');?>">Logout</a></li>
                                </ul>
                        </div>
                        <!-- user dropdown ends -->
                </div>
        </div>
</div>
<!-- topbar ends -->
<div class="container-fluid">
    <div class="row-fluid">
            <!-- left menu starts -->
            <div class="span2 main-menu-span">
                    <div class="well nav-collapse sidebar-nav">
                            <ul class="nav nav-tabs nav-stacked main-menu">
                                    <li class="nav-header hidden-tablet">Weather widget</li>
                                    <li><a href="<?php echo URL::site('admin/dashboard/cities_list');?>"><i class="icon-list-alt"></i><span class="hidden-tablet"> Cities list</span></a></li>
                                    <li><a href="<?php echo URL::site('admin/dashboard/add_city');?>"><i class="icon-plus"></i><span class="hidden-tablet"> Add city</span></a></li>
                                    <li><a href="<?php echo URL::site('admin/dashboard/configuration');?>"><i class="icon-cog"></i><span class="hidden-tablet"> Configuration</span></a></li>
                            </ul>
                    </div><!--/.well -->
            </div><!--/span-->
            <!-- left menu ends -->
            
            <div id="content" class="span10">
                <!-- content starts -->
                <div>
                        <ul class="breadcrumb">
                                <li>
                                        <a href="#">Home</a> <span class="divider">/</span>
                                </li>
                                <li>
                                        <a href="#">Dashboard</a>
                                </li>
                        </ul>
                </div>
                <div class="row-fluid">
                    <noscript>
                            <div class="alert alert-block span12">
                                    <h4 class="alert-heading">Warning!</h4>
                                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                            </div>
                    </noscript>
                </div>
                <?php if(Flash::has_messages('success')):?>
                <div class="row-fluid">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <?php foreach(Flash::get_messages('success') as $m):?>
                        <strong>Success!</strong> <?php echo $m;?>
                        <?php endforeach;?>
                    </div> 
                </div>
                <?php endif;?>
                <?php if(Flash::has_messages('error')):?>
                <div class="row-fluid">
                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <?php foreach(Flash::get_messages('error') as $m):?>
                        <strong>Error!</strong> <?php echo $m;?>
                        <?php endforeach;?>
                    </div> 
                </div>
                <?php endif;?>
                <?php if(Flash::has_messages('warning')):?>
                <div class="row-fluid">
                    <div class="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <?php foreach(Flash::get_messages('warning') as $m):?>
                        <strong>Warning!</strong> <?php echo $m;?>
                        <?php endforeach;?>
                    </div> 
                </div>
                <?php endif;?>
                <div class="row-fluid">
                        <?php echo isset($view) ? $view : '';?>
                </div>
                <!-- content ends -->
            </div><!--/#content.span10-->
        </div><!--/fluid-row-->
        <hr>
        <div class="modal hide fade" id="myModal">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3>Settings</h3>
                </div>
                <div class="modal-body">
                        <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                        <a href="#" class="btn" data-dismiss="modal">Close</a>
                        <a href="#" class="btn btn-primary">Save changes</a>
                </div>
        </div>

        <footer>
            <p class="pull-left">&copy; <a href="<?php echo URL::site('admin/dashboard');?>" target="_blank">WeatherInfo</a> 2013</p>
                <p class="pull-right">Powered by: <a href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
        </footer>

</div><!--/.fluid-container-->
