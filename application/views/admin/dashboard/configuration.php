<div class="box span12">
        <div class="box-header well">
                <h2><i class="icon-cog"></i> System configuration</h2>
                <div class="box-icon">
                </div>
        </div>
        <div class="box-content">
                <?php echo Form::open('admin/dashboard/save_configuration', array('class'=>'form-horizontal', 'data-valid'=>'true', 'data-valid-type'=>'configuration', 'data-valid-strategy'=>'default'));?>
                    <fieldset>
                          <div class="control-group <?php echo Arr::get($errors, 'service_url')? 'error' : '';?>">
                                <label class="control-label" for="service_url">Service URL</label>
                                <div class="controls">
                                    <input class="input-xlarge" id="service_url" name="service_url" type="text" value="<?php echo Arr::get($config, 'service_url');?>">
                                    <?php if (Arr::get($errors, 'service_url')):?>
                                    <span class="error_msg"><?php echo Arr::get($errors, 'service_url');?></span>
                                    <?php endif;?>
                                </div>
                          </div>
                          <div class="control-group <?php echo Arr::get($errors, 'soap_timeout')? 'error' : '';?>">
                                <label class="control-label" for="soap_timeout">Soap Timeout</label>
                                <div class="controls">
                                  <input class="input-xlarge" id="soap_timeout" name="soap_timeout" type="text" value="<?php echo Arr::get($config, 'soap_timeout');?>">
                                  <?php if (Arr::get($errors, 'soap_timeout')):?>
                                  <span class="error_msg"><?php echo Arr::get($errors, 'soap_timeout');?></span>
                                  <?php endif;?>
                                </div>
                          </div>
                          <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                    </fieldset>
                <?php echo Form::close();?>
        </div>
</div>

