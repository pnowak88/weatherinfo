<div class="box span12">
        <div class="box-header well">
                <h2><i class="icon-plus"></i> Add new city</h2>
                <div class="box-icon">
                </div>
        </div>
        <div class="box-content">
                <?php echo Form::open('admin/dashboard/save_city/'.(isset($city) && $city->loaded() ? $city->get_id():''), array('class'=>'form-horizontal', 'data-valid'=>'true', 'data-valid-type'=>'city', 'data-valid-strategy'=>'default'));?>
                    <fieldset>
                          <div class="control-group <?php echo Arr::get($errors, 'name')? 'error' : '';?>">
                            <label class="control-label" for="name">City name <img src="<?php echo URL::base();?>assets/frontend/img/ajax-loader-1.gif" class="hidde-element"></label>
                            <div class="controls">
                                  <input type="text" class="span6 typeahead" id="name" name="name" value="<?php echo Arr::get($values, 'name');?>">
                                  <p class="help-block">Start typing country name to activate cities auto complete.</p>
                                  <?php if (Arr::get($errors, 'name')):?>
                                  <span class="error_msg"><?php echo Arr::get($errors, 'name');?></span>
                                  <?php endif;?>
                            </div>
                          </div>
                          <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                    </fieldset>
                <?php echo Form::close();?>
        </div>
</div>
