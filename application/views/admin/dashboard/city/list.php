<div class="box span12">
        <div class="box-header well">
                <h2><i class="icon-list"></i> Cities List</h2>
                <div class="box-icon">
                </div>
        </div>
        <div class="box-content">
            <a href="<?php echo URL::site('admin/dashboard/add_city');?>" class="btn btn btn-success">Add city</a>
        </div>
        <div class="box-content">
                <table class="table table-bordered table-striped table-condensed">
                        <thead>
                                <tr>
                                        <th>No.</th>
                                        <th>City name</th>
                                        <th>Status</th>
                                        <th class="center">Action</th>                                          
                                </tr>
                        </thead>   
                        <tbody>
                              <?php foreach($cities as $index=>$c):?>
                              <tr>
                                      <td><?php echo ++$index;?></td>
                                      <td class="center"><?php echo $c->get_name();?></td>
                                      <td class="center">
                                          <a href="<?php echo URL::site('admin/city/toggle_active/'.$c->get_id());?>">
                                                <?php if($c->is_active()):?>
                                                <span class="label label-success">Active</span>
                                                <?php else:?>
                                                <span class="label">Inactive</span>
                                                <?php endif;?>
                                          </a>
                                      </td>
                                      <td class="center">
                                          <a class="btn btn-inverse" href="<?php echo URL::site('admin/dashboard/edit_city/'.$c->get_id());?>"><i class="icon-edit icon-white"></i>Edit</a>
                                          <a class="btn btn-danger" href="<?php echo URL::site('admin/city/delete/'.$c->get_id());?>" data-event="delete_city"><i class="icon-edit icon-white"></i>Delete</a>
                                      </td>                                       
                              </tr> 
                              <?php endforeach;?>
                        </tbody>
               </table>
               <a href="<?php echo URL::site('admin/dashboard/add_city');?>" class="btn btn btn-success">Add city</a>
        </div>
</div>
<div class="modal hide fade" id="myModal" style="display: none;">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Delete city</h3>
        </div>
        <div class="modal-body">
                <p>Do you really want to delete this city?</p>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">No</a>
            <a href="<?php echo URL::site('admin/city/delete/');?>" class="btn btn-danger">Yes</a>
        </div>
</div>