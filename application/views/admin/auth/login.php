	<div class="container-fluid">
		<div class="row-fluid">
			<div class="row-fluid">
				<div class="span12 center login-header">
					<a class="brand" href="<?php echo URL::site('admin/dashboard');?>"> 
                                            <img alt="Charisma Logo" src="<?php echo URL::base();?>assets/backend/img/wi-logo.png" /> 
                                            <span>WeatherInfo</span>
                                        </a>
				</div><!--/span-->
			</div><!--/row-->
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<div class="alert alert-info">
						Please login with your Username and Password.
					</div>
                                        <?php echo Form::open('admin/auth/do_login', array('class'=>'form-horizontal', 'data-valid'=>'true', 'data-valid-type'=>'login', 'data-valid-strategy'=>'default'));?>
						<fieldset>
							<div class="input-prepend <?php echo Arr::get($errors, 'username') ? 'error' : '';?>" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="username" id="username" type="text" placeholder="username" />
                                                                <?php if (Arr::get($errors, 'username')):?>
                                                                <span class="error_msg"><?php echo Arr::get($errors, 'username');?></span>
                                                                <?php endif;?>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend <?php echo Arr::get($errors, 'password') ? 'error' : '';?>" title="Password" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" placeholder="password" />
                                                                <?php if (Arr::get($errors, 'password')):?>
                                                                <span class="error_msg"><?php echo Arr::get($errors, 'password');?></span>
                                                                <?php endif;?>
							</div>
							<div class="clearfix"></div>

							<p class="center span5">
							<button type="submit" class="btn btn-primary">Login</button>
							</p>
						</fieldset>
					<?php echo Form::close();?>
				</div><!--/span-->
			</div><!--/row-->
				</div><!--/fluid-row-->
	</div><!--/.fluid-container-->
