<!DOCTYPE html>
<html lang="en">
<head>
	<!--
		Charisma v1.0.0

		Copyright 2012 Muhammad Usman
		Licensed under the Apache License v2.0
		http://www.apache.org/licenses/LICENSE-2.0

		http://usman.it
		http://twitter.com/halalit_usman
	-->
	<meta charset="utf-8">
	<title>WeatherInfo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Paweł Nowak">

	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
        
	<script>
            var WeatherInfo    = <?php echo Javascript::instance()->get_serialized_data(); ?>
        </script>
        <?php foreach($assets->get('head') as $asset) echo $asset; ?>  	
</head>

<body>
	<?php echo isset($content) ? $content : '';?>
        <?php foreach($assets->get('body') as $asset) echo $asset; ?>  	
</body>
</html>

