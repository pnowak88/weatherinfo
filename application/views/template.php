<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>WeatherInfo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <script>
            var WeatherInfo    = <?php echo Javascript::instance()->get_serialized_data(); ?>
    </script>
    <?php foreach($assets->get('head') as $asset) echo $asset; ?>  	

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>

    <div class="container">
        <div class="masthead">
            <ul class="nav nav-pills pull-right">
                <li class="active"><a href="<?php echo URL::site('/');?>">Home</a></li>
                <?php if(Auth::instance()->logged_in()):?>
                    <li><a href="<?php echo URL::site('admin/dashboard');?>">Admin</a></li>
                <?php else:?>
                    <li><a href="<?php echo URL::site('admin/auth/login');?>">Login</a></li>
                <?php endif;?>
            </ul>
            <a class="brand" href="<?php echo URL::site('/');?>"> 
                <img alt="Charisma Logo" src="<?php echo URL::base();?>assets/backend/img/wi-logo.png" /> 
            </a>
            <h3 class="muted">WeatherInfo</h3>
        </div>
        <hr/>
      <!-- Jumbotron -->
      <div class="jumbotron">
        <h1>FP recruitment stuff!</h1>
      </div>

      <hr>

      <?php echo isset($content) ? $content : '';?>

      <hr>

      <div class="footer">
        <p>&copy; WeatherInfo 2013</p>
      </div>

    </div> <!-- /container -->
    <?php foreach($assets->get('body') as $asset) echo $asset; ?>
  </body>
</html>

